\begin{thebibliography}{10}
\expandafter\ifx\csname url\endcsname\relax
  \def\url#1{\texttt{#1}}\fi
\expandafter\ifx\csname urlprefix\endcsname\relax\def\urlprefix{URL }\fi
\expandafter\ifx\csname href\endcsname\relax
  \def\href#1#2{#2} \def\path#1{#1}\fi

\bibitem{ref1}
R.~S. {dos Anjos}, T.~G.~M. {Lourenço}, S.~P. {Ribeiro}, R.~P.~S. {Leão},
  R.~F. {Sampaio}, G.~C. {Barroso}, Sigee - a power quality data management
  software, in: 2012 10th IEEE/IAS International Conference on Industry
  Applications, 2012, pp. 1--5.
\newblock \href {https://doi.org/10.1109/INDUSCON.2012.6453250}
  {\path{doi:10.1109/INDUSCON.2012.6453250}}.

\bibitem{ref2}
Y.~{Wu}, Scientific management - the first step of building energy efficiency,
  in: 2009 International Conference on Information Management, Innovation
  Management and Industrial Engineering, Vol.~4, 2009, pp. 619--622.
\newblock \href {https://doi.org/10.1109/ICIII.2009.608}
  {\path{doi:10.1109/ICIII.2009.608}}.

\bibitem{ref3}
P.~Gama, A.~Oliveira, Conserva{\c{c}}{\~a}o de energia e sua rela{\c{c}}{\~a}o
  com a qualidade de energia el{\'e}trica, XV SNPTEE--Semin{\'a}rio Nacional de
  Produ{\c{c}}{\~a}o e Transmiss{\~a}o de Energia El{\'e}trica. Anais... Foz do
  Igua{\c{c}}u (1999).

\bibitem{ref4}
C.~H. Duarte, R.~Schaeffer,
  \href{http://www.sciencedirect.com/science/article/pii/S0360544210001660}{Economic
  impacts of power electronics on electricity distribution systems}, Energy
  35~(10) (2010) 4010 -- 4015.
\newblock \href {https://doi.org/https://doi.org/10.1016/j.energy.2010.03.037}
  {\path{doi:https://doi.org/10.1016/j.energy.2010.03.037}}.
\newline\urlprefix\url{http://www.sciencedirect.com/science/article/pii/S0360544210001660}

\bibitem{ref5}
A.~{Prudenzi}, V.~{Caracciolo}, A.~{Silvestri}, Electrical load analysis in a
  hospital complex, in: 2009 IEEE Bucharest PowerTech, 2009, pp. 1--6.
\newblock \href {https://doi.org/10.1109/PTC.2009.5281797}
  {\path{doi:10.1109/PTC.2009.5281797}}.

\bibitem{ref6}
M.~S. Ortmann, Filtro ativo trifásico com controle vetorial utilizando {DSP}:
  projeto e implementação, Master's thesis, Universidade Federal de Santa
  Catarina (2008).

\bibitem{ref7}
E.~{Gordo}, A.~{Campos}, D.~{Coelho}, Energy efficiency in a hospital building
  case study: Hospitais da universidade de coimbra, in: Proceedings of the 2011
  3rd International Youth Conference on Energetics (IYCE), 2011, pp. 1--6.

\bibitem{ref8}
S.~{Śmiech}, M.~{Papież}, Energy consumption and economic growth in the light
  of meeting the targets of energy policy in the eu, in: 11th International
  Conference on the European Energy Market (EEM14), 2014, pp. 1--5.
\newblock \href {https://doi.org/10.1109/EEM.2014.6861217}
  {\path{doi:10.1109/EEM.2014.6861217}}.

\bibitem{ref9}
J.~{Wang}, X.~{Liang}, The forecast of energy demand on artificial neural
  network, in: 2009 International Conference on Artificial Intelligence and
  Computational Intelligence, Vol.~3, 2009, pp. 31--35.
\newblock \href {https://doi.org/10.1109/AICI.2009.93}
  {\path{doi:10.1109/AICI.2009.93}}.

\bibitem{ref10}
G.~I. Ruas, T.~A. Bragatto, M.~V. Lamar, A.~R. Aoki, S.~M. de~Rocco,
  Previs{\~a}o de demanda de energia el{\'e}trica utilizando redes neurais
  artificiais e support vector regression (2004).

\bibitem{ref11}
L.~C. M.~d. Andrade, M.~Oleskovicz, Redes neurais artificiais dinâmicas
  aplicadas na previsão de demanda de energia elétrica no curtíssimo prazo,
  in: Simpósio Brasileiro de Sistemas Elétricos - SBSE, UNIOESTE/UTFPR, 2014.

\bibitem{ref12}
{UFTM}, \url{http://www2.ebserh.gov.br/web/hc-uftm/historia}, accessed:
  2020-02-08.

\bibitem{ref13}
N.~{Chayopitak}, D.~G. {Taylor}, Fourier series methods for optimal periodic
  position control, in: Proceedings of the 45th IEEE Conference on Decision and
  Control, 2006, pp. 1221--1226.
\newblock \href {https://doi.org/10.1109/CDC.2006.377153}
  {\path{doi:10.1109/CDC.2006.377153}}.

\bibitem{ref19}
J.~{Attia}, P.~{Cofie}, {Wei-Jen Lee}, M.~{Ladd}, Power quality and reliability
  of university campus equipment - industry and academic partnership program,
  in: Proceedings Frontiers in Education 35th Annual Conference, 2005, pp.
  T4H--T4H.
\newblock \href {https://doi.org/10.1109/FIE.2005.1611994}
  {\path{doi:10.1109/FIE.2005.1611994}}.

\bibitem{ref14}
Fluke,
  \href{https://dam-assets.fluke.com/s3fs-public/1735____umpor0200.pdf}{1735
  Power Log: Manua de Usuário}, rev. 2, 3/10 (Portuguese) (3 2006).
\newline\urlprefix\url{https://dam-assets.fluke.com/s3fs-public/1735____umpor0200.pdf}

\bibitem{ref15}
C.~Spörl, E.~Castro, A.~Luchiari, AplicaÇÃo de redes neurais artificiais na
  construÇÃo de modelos de fragilidade ambiental, Revista do Departamento de
  Geografia 21 (2011) 113--135.

\bibitem{ref18}
F.~J.~V. Zuben, Redes neurais recorrentes (parte 1),
  \url{ftp://ftp.dca.fee.unicamp.br/pub/docs/vonzuben/ia353_1s13/topico5_1s2013.pdf},
  accessed: 2020-02-08.

\bibitem{ref16}
E.~Agirre-Basurko, G.~Ibarra-Berastegi, I.~Madariaga, Regression and multilayer
  perceptron-based models to forecast hourly o3 and no2 levels in the bilbao
  area, Environmental Modelling \& Software 21~(4) (2006) 430--446.

\bibitem{ref17}
S.~Haykin, Redes Neurais: Princ{\'\i}pios e Pr{\'a}tica, Artmed, 2007.

\bibitem{ref20}
Agência Nacional De Energia Elétrica,
  \href{https://www.aneel.gov.br/modulo-8}{Procedimentos de Distribuição de
  Energia Elétrica no Sistema Elétrico Nacional - Qualidade da Energia
  Elétrica}, rev. 10, Resolução Normativa nº 794/2017 (Portuguese) (2018).
\newline\urlprefix\url{https://www.aneel.gov.br/modulo-8}

\end{thebibliography}
